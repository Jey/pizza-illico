﻿using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Models.Utils;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.ViewModels;
using System.Collections.Generic;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Models
{
    class UserModel
    {
        private static IDialogMessageService dialogMessageService = DependencyService.Get<IDialogMessageService>();
        public static string AccessToken { get; set; }
        public static string RefreshToken { get; set; }
        public static List<CartEntry> Cart { get; set; }
        public static CartViewModel CartViewModelRef { get; set; }


        public static void AddInCart(CartEntry entry)
        {
            dialogMessageService.ShowStandardConfirmationDialogAsync("La pizza " + entry.Pizza.Name + " a bien été ajouté au panier.");
            Cart.Add(entry);
            if(CartViewModelRef != null )
                CartViewModelRef.AddInListViewCart(entry);
        }

        /**
         * The bool input "notifyUser" is needed when the client ask to clear all the cart. He knows that every pizzas will
         * be removed, so, send notification is useless. However, when he removes only 1 pizza from the cart,
         * he gets a notification.
         */
        public static void RemoveFromCart(CartEntry entry, bool notifyUser)
        {
            Cart.Remove(entry);
            if (CartViewModelRef != null)
                CartViewModelRef.RemoveFromListViewCart(entry);

            if (notifyUser)
                dialogMessageService.ShowStandardConfirmationDialogAsync("La pizza " + entry.Pizza.Name + " a bien été supprimé du panier.");
        }

        public static void Init(string accessToken, string refreshToken)
        {
            AccessToken = accessToken;
            RefreshToken = refreshToken;
            Cart = new List<CartEntry>();
        }

        /**
         * Client can orders pizzas from differents shops. If the request order success for a shop, the shop's pizzas
         * in cart will be remove. However, the shop's pizzas for which the request fail will not be removed.
         * 
         */
        public static void RemoveAllPizzasByShopId(long shopId)
        {
            List<CartEntry> entriesToRemoveList = new();
            foreach (CartEntry entry in Cart) // Search for pizzas to remove
            {
                if (entry.Shop.Id == shopId)
                {
                    entriesToRemoveList.Add(entry); // Keep reference to pizza to remove
                }
            }

            foreach(CartEntry entry in entriesToRemoveList) // Remove pizza
            {
                RemoveFromCart(entry, false);
            }
        }

        public static void ClearCart()
        {
            Cart.Clear();
            if (CartViewModelRef != null)
                CartViewModelRef.RemoveAllCart();
        }
    }
}
