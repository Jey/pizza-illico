﻿using PizzaIllico.Mobile.Dtos.Pizzas;

namespace PizzaIllico.Mobile.Models.Utils
{
    class CartEntry
    {
        public ShopItem Shop { get; set; }
        public PizzaItem Pizza { get; set; }

        public CartEntry(ShopItem shop, PizzaItem pizza)
        {
            Shop = shop;
            Pizza = pizza;
        }
    }
}
