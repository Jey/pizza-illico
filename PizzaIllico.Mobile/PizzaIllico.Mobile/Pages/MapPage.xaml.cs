﻿using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
        public MapPage()
        {
            // Create objects to fix the map's start point.
            Xamarin.Essentials.Location loc = DependencyService.Get<IGeolocalisationService>().GetCurrentLocation().Result;
            Position center = new Position(loc.Latitude, loc.Longitude);

            // But MapSpan doesn't work...
            Map map = new Map(new MapSpan(center, 0.01, 0.01));
            Content = map;

            BindingContext = new MapViewModel();
            InitializeComponent();
        }
    }
}