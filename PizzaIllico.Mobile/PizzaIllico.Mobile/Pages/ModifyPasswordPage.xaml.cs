﻿using PizzaIllico.Mobile.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModifyPasswordPage : ContentPage
    {
        public ModifyPasswordPage()
        {
            BindingContext = new ModifyPasswordViewModel();
            InitializeComponent();
        }
    }
}