﻿using PizzaIllico.Mobile.ViewModels;
using Storm.Mvvm.Services;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PizzaListPage : ContentPage
    {
        public PizzaListPage()
        {
            BindingContext = new PizzaListViewModel();
            InitializeComponent();
        }

        async void OnCartClicked(object sender, EventArgs e)
        {
            await DependencyService.Get<INavigationService>().PushAsync<CartPage>();
        }

        async void OnSettingsClicked(object sender, EventArgs e)
        {
            await DependencyService.Get<INavigationService>().PushAsync<UserPage>();
        }
    }
}