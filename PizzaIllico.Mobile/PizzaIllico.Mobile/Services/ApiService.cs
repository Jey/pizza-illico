using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PizzaIllico.Mobile.Models;

namespace PizzaIllico.Mobile.Services
{
    public interface IApiService
    {
        Task<TResponse> Get<TResponse>(string url);
        Task<TResponse> Post<TResponse>(string url, string requestBody);
        Task<TResponse> Patch<TResponse>(string url, string requestBody);
    }

    public class ApiService : IApiService
    {
        private const string HOST = "https://pizza.julienmialon.ovh/";
        private readonly HttpClient _client = new HttpClient();

        public async Task<TResponse> Get<TResponse>(string url)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, HOST + url);
            request.Headers.Add("Authorization", "Bearer " + getAccessToken());

            HttpResponseMessage response = await _client.SendAsync(request);

            string content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<TResponse>(content);
        }

        public async Task<TResponse> Post<TResponse>(string url, string requestBody)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, HOST + url);
            request.Headers.Add("Authorization", "Bearer " + getAccessToken());
            request.Content = new StringContent(requestBody, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _client.SendAsync(request);

            string content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<TResponse>(content);
        }

        public async Task<TResponse> Patch<TResponse>(string url, string requestBody)
        {
            HttpRequestMessage request = new HttpRequestMessage(new HttpMethod("PATCH"), HOST + url);
            request.Headers.Add("Authorization", "Bearer " + getAccessToken());
            request.Content = new StringContent(requestBody, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _client.SendAsync(request);

            string content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<TResponse>(content);
        }

        private string getAccessToken()
        {
            return ((UserModel.AccessToken == null) ? "" : UserModel.AccessToken);
        }
    }
}