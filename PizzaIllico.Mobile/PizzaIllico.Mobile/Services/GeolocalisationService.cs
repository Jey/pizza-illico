﻿using System.Threading.Tasks;
using Xamarin.Essentials;

namespace PizzaIllico.Mobile.Services
{

    public interface IGeolocalisationService
    {
        public Task<Location> GetCurrentLocation();
    }

    class GeolocalisationService : IGeolocalisationService
    {

        public async Task<Location> GetCurrentLocation()
        {
            return await Geolocation.GetLastKnownLocationAsync();
        }
    }
}
