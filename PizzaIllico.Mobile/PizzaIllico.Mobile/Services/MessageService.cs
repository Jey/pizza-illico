﻿using PizzaIllico.Mobile.Dtos;
using System.Threading.Tasks;

namespace PizzaIllico.Mobile.Services
{

    public interface IDialogMessageService
    {
        Task ShowDialogAsync(string title, string mainMessage, string okMessage);
        Task ShowStandardConfirmationDialogAsync(string mainMessage);
        Task ShowStandardErrorDialogAsync(string mainMessage);
        Task ShowDialogErrorResponseAsync(Response response);
    }

    public class DialogMessageService : IDialogMessageService
    {
        public async Task ShowDialogAsync(string title, string mainMessage, string okMessage)
        {
            await App.Current.MainPage.DisplayAlert(title, mainMessage, okMessage);
        }
        
        public async Task ShowStandardConfirmationDialogAsync(string mainMessage)
        {
            await App.Current.MainPage.DisplayAlert("Confirmation", mainMessage, "Ok");
        }

        public async Task ShowStandardErrorDialogAsync(string mainMessage)
        {
            await App.Current.MainPage.DisplayAlert("Erreur", mainMessage, "Ok");
        }

        public async Task ShowDialogErrorResponseAsync(Response response)
        {
            await App.Current.MainPage.DisplayAlert("Erreur", "Une erreur s'est produite.\n" +
                    "Code d'erreur : " + response.ErrorCode + "\nMessage d'erreur : " + response.ErrorMessage, "Ok");
        }
    }
}
