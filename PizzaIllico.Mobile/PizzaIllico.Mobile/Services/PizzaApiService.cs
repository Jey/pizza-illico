using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Models;
using PizzaIllico.Mobile.Models.Utils;
using PizzaIllico.Mobile.Utils;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Services
{
    public interface IPizzaApiService
    {
        Task<Response<List<ShopItem>>> ListShops();
        Task<Response<List<PizzaItem>>> ListPizzas(long shopId);
        Task<Response<List<OrderItem>>> GetOrderHistory();
        Task<List<Response<OrderItem>>> DoOrder();
    }
    
    public class PizzaApiService : IPizzaApiService
    {
        private readonly IApiService _apiService;

        public PizzaApiService()
        {
            _apiService = DependencyService.Get<IApiService>();
        }

        public async Task<Response<List<ShopItem>>> ListShops()
        {
	        return await _apiService.Get<Response<List<ShopItem>>>(Urls.LIST_SHOPS);
        }

        public async Task<Response<List<PizzaItem>>> ListPizzas(long shopId)
        {
            string urlBuilt = Util.FindAndReplace(Urls.LIST_PIZZA, "{shopId}", shopId.ToString() );
            return await _apiService.Get<Response<List<PizzaItem>>>(urlBuilt);
        }

        public async Task<Response<List<OrderItem>>> GetOrderHistory()
        {
            return await _apiService.Get<Response<List<OrderItem>>>(Urls.LIST_ORDERS);
        }

        public async Task<List<Response<OrderItem>>> DoOrder()
        {
            Dictionary<long, List<long>> pizzasByRestaurant = new Dictionary<long, List<long>>();
            // Regroup pizzas from the cart. Indeed, the pizzas are disorganized, according to client actions
            foreach ( CartEntry entry in UserModel.Cart )
            {
                if( pizzasByRestaurant.ContainsKey(entry.Shop.Id) ) // If the list already exists
                {
                    pizzasByRestaurant[entry.Shop.Id].Add(entry.Pizza.Id);
                }
                else // Otherwise, create the list first
                {
                    pizzasByRestaurant.Add(entry.Shop.Id, new List<long>());
                    pizzasByRestaurant[entry.Shop.Id].Add(entry.Pizza.Id);
                }
            }

            // Create foreach loop to send shop by shop the orders
            List<Response<OrderItem>> responseList = new List<Response<OrderItem>>();
            foreach (long shopId in pizzasByRestaurant.Keys)
            {
                CreateOrderRequest request = new CreateOrderRequest
                {
                    PizzaIds = pizzasByRestaurant[shopId]
                };

                string urlBuilt = Util.FindAndReplace(Urls.DO_ORDER, "{shopId}", shopId.ToString());
                responseList.Add(await _apiService.Post<Response<OrderItem>>(urlBuilt, JsonConvert.SerializeObject(request)));
            }
            return responseList;
        }
    }
}