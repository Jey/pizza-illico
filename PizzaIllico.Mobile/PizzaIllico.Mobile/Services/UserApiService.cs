﻿using Newtonsoft.Json;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Accounts;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Dtos.Authentications.Credentials;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Services
{
    public interface IUserApiService
    {
        Task<Response<LoginResponse>> CreateNewUser(string email, string firstName, string lastName, string phoneNumber, string password);
        Task<Response<LoginResponse>> LoginUser(string email, string password);
        Task<Response<UserProfileResponse>> GetUserInformations();
        Task<Response> SetNewPassword(string oldPassword, string newPassword);
        Task<Response> UpdateUserSettings(string email, string firstName, string lastName, string phoneNumber);
    }

    class UserApiService : IUserApiService
    {
        private readonly IApiService _apiService;
        private readonly string _clientId = "MOBILE";
        private readonly string _clientSecret = "UNIV";

        public UserApiService()
        {
            _apiService = DependencyService.Get<IApiService>();
        }

        public async Task<Response<LoginResponse>> CreateNewUser(string email, string firstName, string lastName, string phoneNumber, string password)
        {
            CreateUserRequest request = new CreateUserRequest
            {
                ClientId = _clientId,
                ClientSecret = _clientSecret,
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                PhoneNumber = phoneNumber,
                Password = password
            };

            return await _apiService.Post<Response<LoginResponse>>(Urls.CREATE_USER, JsonConvert.SerializeObject(request));
        }

        public async Task<Response<LoginResponse>> LoginUser(string email, string password)
        {
            LoginWithCredentialsRequest request = new LoginWithCredentialsRequest
            {
                Login = email,
                Password = password,
                ClientId = _clientId,
                ClientSecret = _clientSecret
            };

            return await _apiService.Post<Response<LoginResponse>>(Urls.LOGIN_WITH_CREDENTIALS, JsonConvert.SerializeObject(request));
        }

        public async Task<Response<UserProfileResponse>> GetUserInformations()
        {
            return await _apiService.Get<Response<UserProfileResponse>>(Urls.USER_PROFILE);
        }

        public async Task<Response> SetNewPassword(string oldPassword, string newPassword)
        {
            SetPasswordRequest request = new SetPasswordRequest
            {
                OldPassword = oldPassword,
                NewPassword = newPassword
            };

            return await _apiService.Patch<Response>(Urls.SET_PASSWORD, JsonConvert.SerializeObject(request));
        }

        public async Task<Response> UpdateUserSettings(string email, string firstName, string lastName, string phoneNumber)
        {
            SetUserProfileRequest request = new SetUserProfileRequest
            {
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                PhoneNumber = phoneNumber
            };

            return await _apiService.Patch<Response>(Urls.SET_USER_PROFILE, JsonConvert.SerializeObject(request));
        }
    }
}
