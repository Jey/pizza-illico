﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaIllico.Mobile.Utils
{
    static class Util
    {

        public static string FindAndReplace(string source, string find, string replace)
        {
            int place = source.IndexOf(find);
            return source.Remove(place, find.Length).Insert(place, replace);
        }

    }
}
