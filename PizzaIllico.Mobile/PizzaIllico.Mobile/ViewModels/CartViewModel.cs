﻿using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Models;
using PizzaIllico.Mobile.Models.Utils;
using PizzaIllico.Mobile.Services;
using Storm.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    /**
     * CartViewModel has a close relation with UserModel. Indeed, UserModel keep the cart up to date when CartViewModel
     * isn't instantiate, and send him the cart's content in order to CartViewModel display it when needed.
     */
    class CartViewModel : ViewModelBase
    {
        private ObservableCollection<CartEntry> _entryList;
        private double _totalPrice;

        public ICommand RemovePizzaFromCartCommand { get; }
        public ICommand DoOrderCommand { get; }
        public ICommand EmptyCartCommand { get; }

        public ObservableCollection<CartEntry> EntryList
        {
            get => _entryList;
            set => SetProperty(ref _entryList, value);
        }

        public double TotalPrice
        {
            get => _totalPrice;
            set => SetProperty(ref _totalPrice, value);
        }

        public CartViewModel()
        {
            RemovePizzaFromCartCommand = new Command<CartEntry>(RemovePizzaFromCartAction);
            DoOrderCommand = new Command(DoOrderAction);
            EmptyCartCommand = new Command(EmptyCartAction);

            EntryList = new ObservableCollection<CartEntry>(UserModel.Cart);
            // CartViewModel give its reference to UserModel in order to add item into cart if this class is instantiated.
            // It allows to always keep the cart up to date.
            UserModel.CartViewModelRef = this;

            TotalPrice = GetPrice();
        }

        /**
         * Method called by UserModel. CartViewModel rarely modify the cart itself.
         */
        public void AddInListViewCart(CartEntry entry)
        {
            EntryList.Add(entry);
            TotalPrice = GetPrice();
        }


        /**
         * Method called by UserModel. CartViewModel rarely modify the cart itself.
         */
        public void RemoveFromListViewCart(CartEntry entry)
        {
            EntryList.Remove(entry);
            TotalPrice = GetPrice();
        }


        /**
         * Method called by UserModel. CartViewModel rarely modify the cart itself.
         */
        public void RemoveAllCart()
        {
            EntryList.Clear();
        }

        private double GetPrice()
        {
            TotalPrice = 0;
            foreach (CartEntry entry in EntryList)
                TotalPrice += entry.Pizza.Price;
            return TotalPrice;
        }

        /**
         * Rare method where CartViewModel modify the cart. It's because client ask from its view to clear the cart.
         * So, CartViewModel ask to UserModel to clear its cart too.
         */
        private void RemovePizzaFromCartAction(CartEntry entry)
        {
            UserModel.RemoveFromCart(entry, true);
            EntryList.Remove(entry);

            TotalPrice = GetPrice();
        }
        /**
         * Rare method where CartViewModel modify the cart. It's because client ask from its view to clear one
         * element of the cart. So, CartViewModel ask to UserModel to clear its cart too.
         */
        private void EmptyCartAction()
        {
            TotalPrice = 0;
            UserModel.ClearCart();
        }

        private async void DoOrderAction()
        {
            IDialogMessageService dialogMessageService = DependencyService.Get<IDialogMessageService>();
            if (EntryList.Count == 0)
            {
                await dialogMessageService.ShowStandardErrorDialogAsync("Vous ne pouvez pas passer de commande alors que votre panier est vide.");
                return;
            }

            IPizzaApiService service = DependencyService.Get<IPizzaApiService>();
            List<Response<OrderItem>> responseList = await service.DoOrder();

            bool errorOccurs = false;
            foreach (Response<OrderItem> response in responseList)
            {
                if (response.IsSuccess) // remove pizzas ordered
                {
                    UserModel.RemoveAllPizzasByShopId(response.Data.Shop.Id);
                }
                else // otherwise, pizzas stay in cart.
                {
                    errorOccurs = true;
                }
            }

            if (!errorOccurs)
            {
                await dialogMessageService.ShowDialogAsync("Commande validée", "La commande est validée," +
                    "mais étant donnée qu'il s'agit d'une fausse appli, vous ne recevrez aucune pizza :(\n" +
                    "(Vous avez tout de même été facturé).", "Super !");
            }
            else
            {
                await dialogMessageService.ShowDialogAsync("Oups!", "La commande n'a pas été validée...\n" +
                    "Si des pizzas ont été commandé, elles n'appraissent plus dans le panier.", "Mince !");
            }
        }
    }
}
