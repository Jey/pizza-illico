﻿using PizzaIllico.Mobile.Pages;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    class HomeViewModel : ViewModelBase
	{
		public ICommand LoginCommand { get; }
		public ICommand RegisterCommand { get; }

		public HomeViewModel()
		{
			LoginCommand = new Command(LoginAction);
			RegisterCommand = new Command(RegisterAction);
		}

		private void LoginAction()
		{
			DependencyService.Get<INavigationService>().PushAsync<LoginPage>();
		}

		private void RegisterAction()
		{
			DependencyService.Get<INavigationService>().PushAsync<RegisterPage>();
		}
	}
}
