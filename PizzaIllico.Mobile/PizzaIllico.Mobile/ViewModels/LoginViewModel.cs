﻿using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Models;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    class LoginViewModel : ViewModelBase
    {
        private bool _isRunning = false; // ActivityIndicator's property (binded)

        public string Email { get; set; }
        public string Password { get; set; }
        public ICommand LoginCommand { get; }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public LoginViewModel()
        {
            LoginCommand = new Command(LoginAction);
        }

        private async void LoginAction()
        {
            IsRunning = true;

            IDialogMessageService dialogMessageService = DependencyService.Get<IDialogMessageService>();
            if (Email == null || Password == null || Email.Length == 0 || Password.Length == 0 )
            {
                await dialogMessageService.ShowStandardErrorDialogAsync("Veuillez compléter tous les champs.");
                IsRunning = false;
                return;
            }

            IUserApiService service = DependencyService.Get<IUserApiService>();
            Response<LoginResponse> response = await service.LoginUser(Email, Password);

            if ( response.IsSuccess )
            {
                UserModel.Init(response.Data.AccessToken, response.Data.RefreshToken);

                INavigationService navigationService = DependencyService.Get<INavigationService>();
                await navigationService.PushAsync<MainTabbedPage>();
                IsRunning = false;
            }
            else
            {
                await dialogMessageService.ShowDialogErrorResponseAsync(response);
                IsRunning = false;
            }
        }
    }
}
