﻿using PizzaIllico.Mobile.Controls;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Services;
using Storm.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace PizzaIllico.Mobile.ViewModels
{
    class MapViewModel : ViewModelBase
    {
        private ObservableCollection<BindablePin> _pinList;
        public ObservableCollection<BindablePin> PinList
        {
            get => _pinList;
            set => SetProperty(ref _pinList, value);
        }

        public MapViewModel()
        {
            Build();
        }

        private async void Build()
        {
            IPizzaApiService service = DependencyService.Get<IPizzaApiService>();

            Response<List<ShopItem>> response = await service.ListShops();

            _pinList = new();
            foreach (ShopItem shop in response.Data) // Add every shop as pin
            {
                BindablePin pin = new BindablePin()
                {
                    Position = new Position(shop.Latitude, shop.Longitude),
                    Label = shop.Name
                };
            }
        }
    }
}
