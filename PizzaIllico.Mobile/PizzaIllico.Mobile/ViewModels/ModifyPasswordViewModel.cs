﻿using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Services;
using Storm.Mvvm;
using System.Windows.Input;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    class ModifyPasswordViewModel : ViewModelBase
	{
		private bool _isRunning = false; // ActivityIndicator's property (binded)

		public string NewPasswordEntry1 { get; set; }
		public string NewPasswordEntry2 { get; set; }
		public string OldPassword { get; set; }

		public ICommand ChangePasswordCommand { get; }

		public bool IsRunning
		{
			get => _isRunning;
			set => SetProperty(ref _isRunning, value);
		}

		public ModifyPasswordViewModel()
        {
			ChangePasswordCommand = new Command(ChangePasswordAction);

		}

		private async void ChangePasswordAction()
		{
			IsRunning = true;
			IDialogMessageService dialogMessageService = DependencyService.Get<IDialogMessageService>();

			if (NewPasswordEntry1 == null || NewPasswordEntry2 == null || OldPassword == null ||
				NewPasswordEntry1.Length == 0 || NewPasswordEntry2.Length == 0 || OldPassword.Length == 0)
			{
				await dialogMessageService.ShowStandardErrorDialogAsync("Veuillez compléter tous les champs.");
				IsRunning = false;
				return;
			}
			else if (!NewPasswordEntry1.Equals(NewPasswordEntry2))
			{
				await dialogMessageService.ShowStandardErrorDialogAsync("Les nouveaux mots de passe ne sont pas identique.");
				IsRunning = false;
				return;
			}
			else if (NewPasswordEntry1.Length < 5 || NewPasswordEntry2.Length < 5)
			{
				await dialogMessageService.ShowStandardErrorDialogAsync("Choisissez un nouveau mot de passe plus sécurisé.");
				IsRunning = false;
				return;
			}

			IUserApiService service = DependencyService.Get<IUserApiService>();
			Response response = await service.SetNewPassword(OldPassword, NewPasswordEntry1);

			if (response.IsSuccess)
			{
				await dialogMessageService.ShowStandardConfirmationDialogAsync("Nouveau mot de passe enregistré avec succès.");
			}
			else
			{
				await dialogMessageService.ShowDialogErrorResponseAsync(response);
			}
			IsRunning = false;
		}
	}
}
