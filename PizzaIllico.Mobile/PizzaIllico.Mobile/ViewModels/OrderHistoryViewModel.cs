﻿using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Services;
using Storm.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    class OrderHistoryViewModel : ViewModelBase
    {
		private ObservableCollection<OrderItem> _orderList;

		public ObservableCollection<OrderItem> OrderList
		{
			get => _orderList;
			set => SetProperty(ref _orderList, value);
		}

		public OrderHistoryViewModel()
        {
			Build();
        }

		public async void Build()
		{
			IPizzaApiService service = DependencyService.Get<IPizzaApiService>();

			Response<List<OrderItem>> response = await service.GetOrderHistory();

			if (response.IsSuccess)
			{
				OrderList = new ObservableCollection<OrderItem>(response.Data);
			}
			else
			{
				await DependencyService.Get<IDialogMessageService>().ShowDialogErrorResponseAsync(response);
			}
		}
	}
}
