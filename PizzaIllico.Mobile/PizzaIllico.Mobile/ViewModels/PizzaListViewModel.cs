﻿using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Models;
using PizzaIllico.Mobile.Models.Utils;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.Utils;
using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
	/**
	 * In order to display the image of each pizza in the ListView, this class has two members : pizzaIdCount and _pizzaImageUrlTemplate.
	 * Each pizza has its id incremented throughout the listview making. So, this class increments pizzaIdCount to stay coherent
	 * with the value needed.
	 * The URL built is binded to ListView, which then displays the image in Image's Source property.
	 * 
	 * It's not a proper way to do, but I did not success to modify the URL inside the XAML directly with 
	 * the real pizzaId and shopId.
	 */
	class PizzaListViewModel : ViewModelBase
    {
		private readonly String _pizzaImageUrlTemplate = "https://pizza.julienmialon.ovh/api/v1/shops/{shopId}/pizzas/{pizzaId}/image";
		private int pizzaIdCount = 1;
		private ShopItem _shop;
		private ObservableCollection<PizzaItem> _pizzaList;

		public ICommand AddPizzaToCartCommand { get; }

		public ObservableCollection<PizzaItem> PizzaList
		{	
			get => _pizzaList;
			set => SetProperty(ref _pizzaList, value);
		}
		public string PizzaImgUrl
		{
			get // substitute {shopId} and {pizzaId} by their value and return
			{
				string pizzaImageUrl = (string)_pizzaImageUrlTemplate.Clone();
				pizzaImageUrl = Util.FindAndReplace(pizzaImageUrl, "{shopId}", Shop.Id.ToString());
				pizzaImageUrl = Util.FindAndReplace(pizzaImageUrl, "{pizzaId}", pizzaIdCount++.ToString());
				return pizzaImageUrl;
			}
		}

		[NavigationParameter]
		public ShopItem Shop
		{
			get { return _shop; }
			set { SetProperty<ShopItem>(ref _shop, value); }
		}

		public PizzaListViewModel()
        {
			AddPizzaToCartCommand = new Command<PizzaItem>(AddPizzaToCartAction);
		}

		public async void Build()
		{
			IPizzaApiService service = DependencyService.Get<IPizzaApiService>();

			Response<List<PizzaItem>> response = await service.ListPizzas( Shop.Id );
			if (response.IsSuccess)
			{
				PizzaList = new ObservableCollection<PizzaItem>(response.Data);
			}
			else
			{
				await DependencyService.Get<IDialogMessageService>().ShowDialogErrorResponseAsync(response);
			}
		}

		public override void Initialize(Dictionary<string, object> navigationParameters)
		{
			base.Initialize(navigationParameters);
			Shop = GetNavigationParameter<ShopItem>("Shop");

			Build();
		}

		private void AddPizzaToCartAction(PizzaItem pizzaItem)
        {
			UserModel.AddInCart(new CartEntry(Shop, pizzaItem));
        }
	}
}
