﻿using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Models;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    class RegisterViewModel : ViewModelBase
    {
        private bool _isRunning = false; // ActivityIndicator's property (binded)
        public bool IsRunning {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }
        public string Email { get; set;  }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public ICommand SubmitCommand { get; }

        public RegisterViewModel()
        {
            SubmitCommand = new Command( RegisterAction );
        }

        private async void RegisterAction()
        {
            IsRunning = true;

            IDialogMessageService dialogMessageService = DependencyService.Get<IDialogMessageService>();
            if( Email == null || Password == null || Email.Length == 0 || Password.Length == 0 ||
                FirstName == null || LastName == null || FirstName.Length == 0 || LastName.Length == 0 ||
                PhoneNumber == null || PhoneNumber.Length == 0 )
            {
                await dialogMessageService.ShowStandardErrorDialogAsync("Veuillez compléter tous les champs.");
                IsRunning = false;
                return;
            }

            IUserApiService service = DependencyService.Get<IUserApiService>();

            Response<LoginResponse> response = await service.CreateNewUser( Email, FirstName, LastName, PhoneNumber, Password );
            
            if ( response.IsSuccess )
            {
                UserModel.Init(response.Data.AccessToken, response.Data.RefreshToken);

                await dialogMessageService.ShowStandardConfirmationDialogAsync("Inscription effectuée avec succès!");

                INavigationService navigationService = DependencyService.Get<INavigationService>();
                await navigationService.PushAsync<MainTabbedPage>();

                IsRunning = false;
            }
            else
            {
                await dialogMessageService.ShowDialogErrorResponseAsync(response);
                IsRunning = false;
            }
        }
    }
}
