using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace PizzaIllico.Mobile.ViewModels
{
    public class ShopListViewModel : ViewModelBase
    {
	    private ObservableCollection<ShopItem> _shops;

	    public ObservableCollection<ShopItem> Shops
	    {
		    get => _shops;
		    set => SetProperty(ref _shops, value);
	    }

		public ICommand SelectedCommand { get; }

	    public ShopListViewModel()
	    {
		    SelectedCommand = new Command<ShopItem>(SelectedAction);
	    }

	    private void SelectedAction(ShopItem shop)
	    {
			DependencyService.Get<INavigationService>().PushAsync<PizzaListPage>(
				new Dictionary<string, object>
                {
					{ "Shop", shop }
                }
			);
		}

	    public override async Task OnResume()
        {
	        await base.OnResume();

	        IPizzaApiService pizzaApiService = DependencyService.Get<IPizzaApiService>();
	        Response<List<ShopItem>> response = await pizzaApiService.ListShops();

	        if (response.IsSuccess)
	        {
				Shops = new ObservableCollection<ShopItem>(response.Data);

				// Get distance from user to restaurants
				IGeolocalisationService geolocService = DependencyService.Get<IGeolocalisationService>();
				foreach ( ShopItem shop in Shops )
                {
					Location loc = geolocService.GetCurrentLocation().Result;
					Distance distance = Distance.BetweenPositions(
						new Position(shop.Latitude,shop.Longitude),
						new Position(loc.Latitude, loc.Longitude)
					);
					// Affect the distance into shop's MinutesPerKilometer member.
					shop.MinutesPerKilometer = Math.Ceiling(distance.Kilometers);
				}
			}
        }
    }
}