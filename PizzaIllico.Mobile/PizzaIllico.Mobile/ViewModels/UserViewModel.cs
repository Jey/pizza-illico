﻿using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Accounts;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    class UserViewModel : ViewModelBase
	{
		private bool _isRunning = false; // ActivityIndicator's property (binded)

		private string _email;
		private string _firstName;
		private string _lastName;
		private string _phoneNumber;
		public ICommand ChangePasswordCommand { get; }
		public ICommand UpdateUserCommand { get; }
		public ICommand OrderHistoryCommand { get; }

		public bool IsRunning
		{
			get => _isRunning;
			set => SetProperty(ref _isRunning, value);
		}

		public string Email
		{
			get => _email;
			set => SetProperty(ref _email, value);
		}

		public string FirstName
		{
			get => _firstName;
			set => SetProperty(ref _firstName, value);
		}

		public string LastName
		{
			get => _lastName;
			set => SetProperty(ref _lastName, value);
		}

		public string PhoneNumber
		{
			get => _phoneNumber;
			set => SetProperty(ref _phoneNumber, value);
		}

		public UserViewModel()
        {
			UpdateUserCommand = new Command(UpdateUserAction);
			OrderHistoryCommand = new Command(OrderHistoryAction);
			ChangePasswordCommand = new Command(ChangePasswordAction);
			Build();
		}

		public async void Build()
		{
			IUserApiService service = DependencyService.Get<IUserApiService>();

			Response<UserProfileResponse> response = await service.GetUserInformations();

			if (response.IsSuccess)
			{
				Email = response.Data.Email;
				FirstName = response.Data.FirstName;
				LastName = response.Data.LastName;
				PhoneNumber = response.Data.PhoneNumber;
			}
			else
            {
				await DependencyService.Get<IDialogMessageService>().ShowDialogErrorResponseAsync(response);
				Email = "NO_DATA";
				FirstName = "NO_DATA";
				LastName = "NO_DATA";
				PhoneNumber = "NO_DATA";
			}
		}

		private async void OrderHistoryAction()
		{
			await DependencyService.Get<INavigationService>().PushAsync<OrderHistoryPage>();
		}
		
		private async void ChangePasswordAction()
		{
			await DependencyService.Get<INavigationService>().PushAsync<ModifyPasswordPage>();
		}

		private async void UpdateUserAction()
		{
			IsRunning = true;
			IDialogMessageService dialogMessageService = DependencyService.Get<IDialogMessageService>();

			if (Email == null || FirstName == null || LastName == null || PhoneNumber == null ||
				Email.Length == 0 || FirstName.Length == 0 || LastName.Length == 0 || PhoneNumber.Length == 0)
			{
				IsRunning = false;
				await dialogMessageService.ShowStandardErrorDialogAsync("Veuillez compléter tous les champs.");
				return;
			}

			IUserApiService service = DependencyService.Get<IUserApiService>();
			Response response = await service.UpdateUserSettings(Email, FirstName, LastName, PhoneNumber);

			if (response.IsSuccess)
			{
				await dialogMessageService.ShowStandardConfirmationDialogAsync("Profil mis à jour avec succès.");
			}
			else
			{
				await dialogMessageService.ShowDialogErrorResponseAsync(response);
			}
			IsRunning = false;
		}
	}
}
